
package com.mygdx.game.stage


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.viewport.Viewport
import com.mygdx.game.*
import com.mygdx.game.`interface`.FontTtf
import com.mygdx.game.screens.WallScreen
import com.mygdx.game.screens.QuarryScreen


class StageBgTown(v:Viewport, val game: MainGame): Stage( v, game.batch), FontTtf {

    private val txtureMine: Texture = Texture("textures/mine-1.png")
    private val txtureMine2: Texture = Texture("textures/mine-2.png")
    private val txtureHouse: Texture = Texture("textures/house.png")
    private val txtureHouse2: Texture = Texture("textures/house2.png")
    private val txtureLogo:Texture = game.getTexture(Textures.SIGN)
    val bgTown = game.getTexture(Textures.BGTOWN)
    val filePixfont = Gdx.files.internal("pixFont.ttf")

    var drawButMine = TextureRegionDrawable(TextureRegion(txtureMine))
    var drawButMine2 = TextureRegionDrawable(TextureRegion(txtureMine2))
    var drawButHome = TextureRegionDrawable(TextureRegion(txtureHouse))
    var drawButHome2 = TextureRegionDrawable(TextureRegion(txtureHouse2))
    var drawButGame   = TextureRegionDrawable(TextureRegion(txtureLogo))
    val imgBgTown = Image(bgTown)

    val butMine = Button(drawButMine, drawButMine,drawButMine2)
    val butHouse = Button(drawButHome,drawButHome,drawButHome2)
    val butGame = Button(drawButGame)


    val fontEnter: BitmapFont
    val table= Table()


    init {
        fontEnter = createFont(Color.BLACK, screenWidth/300f,filePixfont)

      // imgBgTown.setSize(bgTown.width*3f* widthScale,bgTown.height*3f*heightScale)
        //imgBgTown.setSize(screenWidth*1f, screenHeight*1f)
        imgBgTown.setSize(screenWidth, screenHeight*1.6667f)



        //
        butHouse.setPosition(0f, screenHeight *.13f)
      butHouse.setSize(txtureHouse.width *2f* widthScale,txtureHouse.height*2f*heightScale )



        butHouse.addListener(object : ClickListener() {
            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int):Boolean{
                if(butHouse.isChecked){
                    game.screen = QuarryScreen(game)
                }
                else {
                    butMine.isChecked=false
                    butHouse.isChecked=false
                }

                return true
            }
        })

        butMine.setPosition(screenWidth *.49f, screenHeight *.45f)
        butMine.setSize(txtureMine.width* widthScale *1.1f,txtureMine.height* heightScale *1.1f)
        butMine.addListener(object : ClickListener() {

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int):Boolean{
                if(butMine.isChecked){
                    game.screen = QuarryScreen(game)
                }
                else {
                    butMine.isChecked=false
                    butHouse.isChecked=false
                }

                return true
            }
        })


        butGame.setPosition(screenWidth*.72f,screenHeight*.02f)
        butGame.setSize(txtureLogo.width*widthScale*2.7f,txtureLogo.height*heightScale*2.7f)



        butGame.addListener(object : ClickListener() {
            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                val screenBf = game.screen
                game.screen = WallScreen(game)
                screenBf.dispose()
                return super.touchDown(event, x, y, pointer, button)


            }
        })

        //table.add(butHouse).width(1000f).height(1000f)
       // addActor(table)
        addActor(imgBgTown)
        addActor(butGame)
        addActor(butMine)
        addActor(butHouse)




    }



    override fun draw() {
        this.act()
        //game.batch.begin()
       // game.batch.draw(bgTown,0f,0f, bgTown.width*3f* widthScale,bgTown.height*3f*heightScale)
       // game.batch.end()
        super.draw()

    }

    override fun dispose() {
    }

}
