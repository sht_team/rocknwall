package com.mygdx.game.stage


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.mygdx.game.*

import com.mygdx.game.screens.ParamScreen

class StageHeader(val game:MainGame): Stage() {


    private val txtureParam:Texture = Texture("textures/param.png")



    var drawButParam   = TextureRegionDrawable(TextureRegion(txtureParam))
    val butParam = ImageButton(drawButParam)

   init{
       butParam.setPosition(screenWidth *.02f, screenHeight *.85f)
       butParam.setSize(txtureParam.width* widthScale *.8f,txtureParam.height* heightScale *.8f)
       butParam.image.setFillParent(true)

       butParam.addListener(object : ClickListener() {
           override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
               val scr = game.screen
               game.screen = ParamScreen(game,scr )
               dispose()
               return true
           }
       })


       addActor(game.headerRessources)
       addActor(butParam)
   }

    override fun draw() {
       this.act(Gdx.graphics.deltaTime)
        //this.act()
        super.draw()
    }
}