package com.mygdx.game.stage

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.screenHeight
import com.mygdx.game.screenWidth


class StageBgWall:Stage(){

    val bgWall = Texture("textures/bgWall.png")

    val imgBg1 = Image(bgWall)
    val imgBg2 = Image(bgWall)

    init {
        imgBg1.setSize(screenWidth *1f, screenHeight *1f)
        imgBg1.setPosition(0f,0f)

        imgBg2.setSize(screenWidth *1f, screenHeight *1f)
        imgBg2.setPosition(0f, -screenHeight *1f)





        addActor(imgBg1)
        addActor(imgBg2)

    }

    fun moveGround() {

        imgBg1.addAction(Actions.moveTo(0f, screenHeight*1f,2f))
        imgBg2.addAction(Actions.moveTo(0f,0f,2f))

    }

    fun initGround() {
        imgBg1.setPosition(0f,0f)
        imgBg2.setPosition(0f, -screenHeight*1f)

    }

    override fun draw() {
       this.act(Gdx.graphics.deltaTime)
        //this.act()
        super.draw()
    }
}