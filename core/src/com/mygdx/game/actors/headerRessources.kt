package com.mygdx.game.actors

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.Textures
import com.mygdx.game.`interface`.FontTtf
import com.mygdx.game.`interface`.loadTextures
import com.mygdx.game.models.Player

class headerRessources(val joueur:Player, textureMap:loadTextures): Actor(), FontTtf {

    private val bgHeader = textureMap.getTexture(Textures.BG_HEADER)
    private val txtureGold = textureMap.getTexture(Textures.GOLD)
    private val txtureDiam = textureMap.getTexture(Textures.DIAM)
    private val txtureBrique = textureMap.getTexture(Textures.BRICK)
    val filePixfont = Gdx.files.internal("pixFont.ttf")

    val imgBgHeader = Image(bgHeader)
    val imgGold = Image(txtureGold)
    val imgDiam = Image(txtureDiam)
    val imgBrique = Image(txtureBrique)


    private val originalWidth = 1080f
    private val originalHeight = 1794f


    var screenWidth =  Gdx.graphics.width
    val screenHeight = Gdx.graphics.height
    val widthScale = screenWidth / originalWidth
    val heightScale = screenHeight / originalHeight
    val df = createFont(Color.WHITE, screenWidth /250f, filePixfont)


    private val blank_space=screenWidth /7f
    private val blankH_space=screenHeight /17f

    val resGold:RessourceDisplay
    val resDiam:RessourceDisplay
    val resBrick:RessourceDisplay


    init {


        imgGold.setSize(txtureGold.width*1.4f* widthScale, txtureGold.height*1.4f* heightScale)
        imgGold.setPosition(screenWidth *.36f, screenHeight *.917f)
        imgDiam.setSize(txtureDiam.width*1.1f* widthScale, txtureDiam.height*1.1f* heightScale)
        imgDiam.setPosition(screenWidth *.65f, screenHeight *.92f)
        imgBrique.setSize(txtureBrique.width*1.1f* widthScale, txtureBrique.height*1.1f* heightScale)
        imgBrique.setPosition(screenWidth *.04f, screenHeight *.916f)

        imgBgHeader.setSize(screenWidth *1f, bgHeader.height*1.15f* heightScale)
        imgBgHeader.setPosition(0f, screenHeight *.915f)

        resGold = RessourceDisplay(df, txtureGold, imgGold, joueur.getNbGold(), imgGold.x,imgGold.y)
        resDiam = RessourceDisplay(df, txtureDiam, imgDiam, joueur.getNbDiam(), imgDiam.x,imgDiam.y)
        resBrick = RessourceDisplay(df, txtureBrique, imgBrique, joueur.getNbBrique(), imgBrique.x,imgBrique.y)
    }






    override fun draw(batch: Batch?, parentAlpha: Float) {
        batch as SpriteBatch
        batch.draw(bgHeader, imgBgHeader.x, imgBgHeader.y, imgBgHeader.width, imgBgHeader.height)
        resBrick.draw(batch,1f)
        resDiam.draw(batch,1f)
        resGold.draw(batch,1f)

        resBrick.updateRes(joueur.getNbBrique())
        resDiam.updateRes(joueur.getNbDiam())
        resGold.updateRes(joueur.getNbGold())

    }



    
}