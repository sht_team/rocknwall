package com.mygdx.game.actors

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.screenHeight
import com.mygdx.game.screenWidth

class RessourceDisplay(val font: BitmapFont, val txtureImg: Texture,val img: Image, nbRes:Int,val posX:Float,val posY:Float): Actor() {

    val blank_space= screenWidth /7f
    val blankH_space= screenHeight /17f
    val posx = posX + blank_space
    val posy = posY + blankH_space
    var _nbRes = nbRes


    override fun draw(batch: Batch, parentAlpha: Float) {
        when {
            _nbRes<100 -> font.draw(batch,_nbRes.toString(),posx,posy)
            _nbRes<1000 -> font.draw(batch,_nbRes.toString(),posx,posy)
            _nbRes<10000 -> font.draw(batch,"${_nbRes/1000}K${_nbRes%1000/10}",posx,posy)
            _nbRes<100000 -> font.draw(batch,"${_nbRes/1000}K${_nbRes%1000/100}",posx,posy)
            _nbRes<1000000 -> font.draw(batch,"${_nbRes/1000}K",posx,posy)
            else -> font.draw(batch,"${_nbRes/1000000}M",posx,posy)
        }



        batch.draw(txtureImg, posX, posY,img.width,img.height)

    }

    fun updateRes(value:Int){
        _nbRes = value
    }




}