package com.mygdx.game.displays

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.mygdx.game.heightScale
import com.mygdx.game.screenHeight
import com.mygdx.game.screenWidth
import com.mygdx.game.screens.NB_BRIQUE
import com.mygdx.game.widthScale


class DisplayWall {

    val barBg = Texture("textures/bar0.png")
    val barFront = Texture("textures/bar1.png")
    val clickSheet = Texture("textures/click_sh.png")
    val goldSheet = Texture("textures/goldSheet.png")
    val btnUpg_up = Texture("textures/btn.png")
    val btnUpg_Down = Texture("textures/btn1.png")

    var clickAnimation: Animation<TextureRegion>
    var goldAnimation: Animation<TextureRegion>
    val imgBarBg= Image(barBg)
    var imgBarStart:Image
    val barStart: TextureRegion
    val drawBtnUpg_up = TextureRegionDrawable(TextureRegion(btnUpg_up))
    val drawBtnUg_down = TextureRegionDrawable(TextureRegion(btnUpg_Down))
    var upButton:ImageButton

    var fillBar = 0
    var x_click = 0f
    var y_click = 0f
    var stateTime = 0f
    var stateTime2 = 0f
    var click_anim = false
    var gold_anim = false
    var collect_anim = false
    //val font = createFont(Color.GOLD,screenWidth /200f,filePixfont)

    init {
        fillBar = (barBg.width)/ NB_BRIQUE
        imgBarBg.setSize(barBg.width*widthScale,barBg.height*heightScale)
        imgBarBg.setPosition(screenWidth/4.2f,screenHeight*.85f)


        barStart = TextureRegion(barFront,0,barFront.height)
        imgBarStart = Image(barStart)

        imgBarStart.setSize(barFront.width*widthScale,barFront.height*heightScale)
        imgBarStart.setPosition(screenWidth/4.2f,screenHeight*.85f)


        val tmp = TextureRegion.split(clickSheet,
                clickSheet.getWidth() / 2,
                clickSheet.getHeight() / 2)

        val clickFrames = arrayOfNulls<TextureRegion>(2 * 2)
        var index = 0
        for (i in 0 until 2) {
            for (j in 0 until 2) {
                clickFrames[index++] = tmp[i][j]
            }
        }

        clickAnimation = Animation<TextureRegion>(0.05f, *clickFrames)

        val tmp2 = TextureRegion.split(goldSheet,
                goldSheet.getWidth() / 3,
                goldSheet.getHeight() / 3)

        val goldFrames = arrayOfNulls<TextureRegion>(3 * 3)
        var index2 = 0
        for (i in 0 until 3) {
            for (j in 0 until 3) {
                goldFrames[index2++] = tmp2[i][j]
            }
        }
        goldAnimation = Animation<TextureRegion>(0.09f, *goldFrames)



        upButton = ImageButton(drawBtnUpg_up)
        upButton.image.setFillParent(true)
        upButton.setSize(btnUpg_up.width*1.5f*widthScale,btnUpg_up.height*1.5f*heightScale)
        upButton.setPosition(screenWidth*.65f,0f)
        upButton.isDisabled=true



    }

    fun progressBar(batch: SpriteBatch, nbBickSet:Int){
        barStart.regionWidth = (fillBar*nbBickSet)

        batch.draw(barBg,imgBarBg.x,imgBarBg.y,imgBarBg.width,imgBarBg.height)
        batch.draw(barStart,imgBarStart.x,imgBarStart.y,barStart.regionWidth*widthScale,imgBarStart.height)

    }

    fun clickAnimation(batch: SpriteBatch){
        stateTime += Gdx.graphics.getDeltaTime()
        val currentFrame = clickAnimation.getKeyFrame(stateTime, false)
        batch.draw(currentFrame,x_click-50f,y_click-50f, currentFrame.regionWidth*widthScale, currentFrame.regionHeight*heightScale)
        if (clickAnimation.isAnimationFinished(stateTime)) {
            click_anim = false
            stateTime = 0f
        }

    }



        fun goldAnimation(batch: SpriteBatch, stg:Stage){

        stateTime += Gdx.graphics.getDeltaTime()
        val currentFrame = goldAnimation.getKeyFrame(stateTime, false)
        batch.draw(currentFrame,screenWidth*.4f,screenHeight*.5f,currentFrame.regionWidth*widthScale, currentFrame.regionHeight*heightScale)
        if(goldAnimation.isAnimationFinished(stateTime)){

            val imgCur = Image(currentFrame)

            stg.addActor(imgCur)
            imgCur.setSize(imgCur.width*widthScale, imgCur.height*heightScale)
            imgCur.setPosition(screenWidth*.4f,screenHeight*.5f)
           // imgCur.addAction(Actions.moveTo(imgGold.x,imgGold.y,.5f))
           imgCur.addAction(Actions.fadeOut(.3f))

            gold_anim = false
            stateTime = 0f


        }

          //  font.draw(batch,"$goldEarned",screenWidth*.57f, screenHeight*.57f)

    }



}