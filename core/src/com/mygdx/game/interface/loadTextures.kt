package com.mygdx.game.`interface`

import com.badlogic.gdx.graphics.Texture
import com.mygdx.game.Textures

interface loadTextures {
    fun getTexture(texture: Textures): Texture
}