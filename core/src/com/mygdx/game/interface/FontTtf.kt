package com.mygdx.game.`interface`

import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

interface FontTtf {


    fun createFont(color: Color, sizef:Float, fileFont: FileHandle): BitmapFont {
        val generator = FreeTypeFontGenerator(fileFont)
        val parameter: FreeTypeFontGenerator.FreeTypeFontParameter = FreeTypeFontGenerator.FreeTypeFontParameter()
        parameter.color = color
        val font = generator.generateFont(parameter)
        font.data.setScale(sizef)
        generator.dispose()
        return font
    }


}