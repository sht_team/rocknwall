package com.mygdx.game.models

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.displays.*
import com.mygdx.game.screenHeight
import com.mygdx.game.screenWidth
import java.lang.IndexOutOfBoundsException

class Wall : Actor() {

    var listBrick:MutableList<Brick> = mutableListOf()
    val imgbrique = Texture("textures/brique2.png")
    val imgPetiteBrique= Texture("textures/littleBrique2.png")
    var theme: Music = Gdx.audio.newMusic(Gdx.files.internal("sounds/button_click1.ogg"))

    private var nbBrickSet = 0
    var isFinished:Boolean = false
    var brickToSet: Int = 0
    var anim = false

    init {
        val depX = screenWidth*.22f
        val depY = screenHeight*.3f


        theme.volume = .5f

        listBrick.add(Brick(imgbrique, depX, depY))

        val heightGap = listBrick[0].getBriqueImg().height*.4f
        val widthGap = listBrick[0].getBriqueImg().width*.95f
        val widthLittleGap = listBrick[0].getBriqueImg().width*.47f


        listBrick.add(Brick(imgbrique, depX + widthGap, depY))
        listBrick.add(Brick(imgbrique, depX + widthGap * 2, depY))
        listBrick.add(Brick(imgbrique, depX + widthGap * 3, depY))
        listBrick.add(Brick(imgPetiteBrique, depX, depY + heightGap))
        listBrick.add(Brick(imgbrique, depX + widthLittleGap, depY + heightGap))
        listBrick.add(Brick(imgbrique, depX + widthGap + widthLittleGap, depY + heightGap))
        listBrick.add(Brick(imgbrique, depX + widthLittleGap + widthGap * 2, depY + heightGap))
        listBrick.add(Brick(imgPetiteBrique, depX + widthLittleGap + widthGap * 3, depY + heightGap))
        listBrick.add( Brick(imgbrique, depX, depY + heightGap * 2))
        listBrick.add(Brick(imgbrique, depX + widthGap, depY + heightGap * 2))
        listBrick.add(Brick(imgbrique, depX + widthGap * 2, depY + heightGap * 2))
        listBrick.add(Brick(imgbrique, depX + widthGap * 3, depY + heightGap * 2))
        listBrick.add(Brick(imgPetiteBrique, depX, depY + heightGap * 3))
        listBrick.add(Brick(imgbrique, depX + widthLittleGap, depY + heightGap * 3))
        listBrick.add(Brick(imgbrique, depX + widthLittleGap + widthGap, depY + heightGap * 3))
        listBrick.add(Brick(imgbrique, depX + widthLittleGap + widthGap * 2, depY + heightGap * 3))
        listBrick.add(Brick(imgPetiteBrique, depX + widthLittleGap + widthGap * 3, depY + heightGap * 3))
        listBrick.add(Brick(imgbrique, depX, depY + heightGap * 4))
        listBrick.add(Brick(imgbrique, depX + widthGap, depY + heightGap * 4))
        listBrick.add(Brick(imgbrique, depX + widthGap * 2, depY + heightGap * 4))
        listBrick.add( Brick(imgbrique, depX + widthGap * 3, depY + heightGap * 4))



    }

    fun getNbBrickSet(): Int {
        return nbBrickSet
    }




    fun putBrick(stg:Stage) {

        for (i in 1..brickToSet) {
            try {
                stg.addActor(listBrick.get(nbBrickSet).getBriqueImg())

            } catch (e: IndexOutOfBoundsException) {
            }
            nbBrickSet++
            theme.play()
            theme.position = 0f
        }

        brickToSet = 0




    }

    fun finishedWall(stg: Stage){
            nbBrickSet = 0
            stg.clear()

           for (brick in listBrick)
               brick.initPos()


            isFinished = false
            anim = false


    }

}