package com.mygdx.game.models

class Level {
    var lvl = 1
    var pwrNeeded = 1
    var remainingClick= 1
    var goldEarned = 3
    var isBoss = false


    fun goNextLevel() {
        lvl++
        pwrNeeded += lvl
        remainingClick = pwrNeeded
        goldEarned += pwrNeeded / 3

    }

    fun click(clickPwr:Int):Int {
        var brickToPut = 0

        remainingClick -= clickPwr


        while (remainingClick <= 0) {
            brickToPut++
            remainingClick+= pwrNeeded

        }
        if ( remainingClick <= 0)
            remainingClick = pwrNeeded

        return  brickToPut

    }



}