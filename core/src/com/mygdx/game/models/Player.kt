package com.mygdx.game.models

//TODO: Changement: see DC
class Player {

    private var nbGold=0
    private var nbDiam=0
    private var nbBrique=0
    private var clickPower=1
    private var lvlPower = 1
    private var upPrice = 3
    private var upPowerlvl = 1


    fun getNbGold(): Int {return nbGold}
    fun setNbGold(_nbG: Int){ this.nbGold = _nbG }
    fun addGold(_nbG:Int ){this.nbGold+= _nbG}
    fun useGold(_nbG:Int ){this.nbGold -= _nbG}

    fun getNbDiam(): Int {return nbDiam}
    fun setNbFiam(_nbD: Int){ this.nbDiam = _nbD }
    fun addDiam(_nbD:Int ){this.nbDiam+= _nbD}
    fun useDiam(_nbD:Int ){this.nbDiam -= _nbD}

    fun getNbBrique(): Int {return nbBrique}
    fun setNbBrique(_nbB: Int){ this.nbBrique = _nbB }
    fun addBrique(_nbB:Int ){this.nbBrique+= _nbB}
    fun useBrique(_nbB:Int ){this.nbBrique -= _nbB}

    fun getClickPower(): Int {return clickPower}
    fun setClickPower(_Pwr: Int){ this.clickPower = _Pwr }

    fun getUpPower(): Int{
        return upPowerlvl
    }
    fun upPower(){
        this.clickPower += upPowerlvl
        upPrice += 5 + (lvlPower*1.3).toInt()
        lvlPower++
        upPowerlvl++

    }

    fun getUpgradePrice(): Int {
        return upPrice
    }


}