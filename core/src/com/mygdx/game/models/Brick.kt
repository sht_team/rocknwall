package com.mygdx.game.models

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.displays.*
import com.mygdx.game.heightScale
import com.mygdx.game.screenHeight
import com.mygdx.game.widthScale


class Brick(txtureImg: Texture, xpos:Float, ypos:Float) : Actor() {

    var posX = 0f
    var posY = 0f
    var initW:Float
    var initH:Float
    var imgBrique:Image


    init {
        posX = xpos
        posY = ypos
        imgBrique = Image(txtureImg)
        initW = imgBrique.width*widthScale*.7f
        initH = imgBrique.height*heightScale*.7f
        imgBrique.setSize(initW,initH)
        initPos()


    }


    fun getBriqueImg():Image {return imgBrique}
    fun initPos() {
        imgBrique.setPosition(posX,posY+ screenHeight*.2f)
        imgBrique.addAction(Actions.moveTo(posX,posY,.15f))
    }



    fun finishedAction() {
        imgBrique.addAction(Actions.moveTo(posX, posY+ screenHeight,2f))
        //imgBrique.addAction(Actions.sequence(Actions.moveTo(posX, posY+ screenHeight*.07f,.4f),Actions.moveTo(posX, posY-screenHeight*.5f,2.5f)))
        //Actions.parallel(Actions.scaleTo(.5f,.5f,.3f),Actions.moveTo(screenWidth*.5f, screenHeight*.5f,.3f)
    }







}