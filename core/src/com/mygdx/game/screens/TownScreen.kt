package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.input.GestureDetector.GestureListener
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.mygdx.game.*
import com.mygdx.game.displays.*
import com.mygdx.game.stage.StageBgTown
import com.mygdx.game.stage.StageHeader
import kotlin.system.exitProcess


class TownScreen(private val game: MainGame) : Screen {

    private var camera: OrthographicCamera = OrthographicCamera()
    private val stageHeader=StageHeader(game)
    private val stageBgTown:StageBgTown
    var gstList:gestureList
    private var timeSeconds = 0f
    private val txtureHouse: Texture = Texture("textures/house.png")
    private val txtureHouse2: Texture = Texture("textures/house2.png")
    var drawButHome = TextureRegionDrawable(TextureRegion(txtureHouse))
    var drawButHome2 = TextureRegionDrawable(TextureRegion(txtureHouse2))
    val butHouse = ImageButton(drawButHome,drawButHome,drawButHome2)
    val imgHouse= Image(txtureHouse)


    init {
        camera.setToOrtho(false, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

        val viewp = FitViewport(Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat(),camera)
        stageBgTown = StageBgTown(viewp,game)
        gstList= gestureList(camera, stageBgTown)
        gstList.panMax = camera.position.y
        gstList.panMax2 = stageBgTown.bgTown.height*1.8f*heightScale

        butHouse.setSize(txtureHouse.width* widthScale *1.85f,txtureHouse.height* heightScale *1.85f)
        butHouse.image.setFillParent(true)



    }


    override fun hide() {
        Gdx.input.inputProcessor = null
    }
    override fun show() {
        val multiplexer = InputMultiplexer()
        Gdx.input.inputProcessor = multiplexer
        multiplexer.addProcessor(GestureDetector(gstList))
        multiplexer.addProcessor(stageHeader)
        multiplexer.addProcessor(stageBgTown)



    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(60/255f, 55/255f, 55/255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined

        timeSeconds +=Gdx.graphics.rawDeltaTime
        if(timeSeconds > period) {
            game.timeLeft ++
            timeSeconds = 0f
        }

        stageBgTown.draw()
        stageHeader.draw()


        gstList.flingDecelerate(delta)
        if(camera.position.y in gstList.panMax..gstList.panMax2 || gstList.delta!=0f) {
            camera.position.y += gstList.delta
            camera.position.y += gstList.flingVelocityY*delta
           camera.update()
        }

        if(gstList.delta!=0f){

        }


        if(camera.position.y < gstList.panMax)
            camera.position.y = gstList.panMax


        if(camera.position.y > gstList.panMax2)
            camera.position.y = gstList.panMax2


        // Exit the game when back button pressed 2 times
       /* if(Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            exitCount++
            if (exitCount>1){ exitProcess(0) }
        }*/
        game.batch.begin()
      // game.batch.draw(txtureHouse,0f,0f,txtureHouse.width*2f* widthScale,txtureHouse.height*2f* heightScale)
        game.batch.end()

    }


    // Override not used
    override fun pause() {}
    override fun resume() {}
    override fun resize(width: Int, height: Int) {}

    override fun dispose() {
        stageBgTown.dispose()
        stageHeader.dispose()

    }
}