package com.mygdx.game.screens


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.mygdx.game.*



class logoScreen(private val game: MainGame) : Screen {

    private var camera: OrthographicCamera = OrthographicCamera()
    private val logo:Texture = game.getTexture(Textures.LOGOSHT)
    private val img: Image = Image(logo)
    private var timeSeconds = 0f
    private val period = .1f
    var a = -.5f


    init {
        camera.setToOrtho(false, screenWidth*1f, screenHeight*1f)
        img.setSize(logo.width*2f*widthScale,logo.height*2f*heightScale)
        img.setPosition(screenWidth*.15f,screenHeight*.4f)

       
    }
    override fun hide() {
    }

    override fun show() {
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(40/255f, 40/255f, 40/255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)


       timeSeconds +=Gdx.graphics.rawDeltaTime
        if(timeSeconds > period) {
            timeSeconds -= period
            a += period
        }
        camera.update()
        game.batch.projectionMatrix = camera.combined
        game.batch.setColor(img.color)
        game.batch.begin()
       img.draw(game.batch,a)
        game.batch.end()

      if (a >= 2f) {
            game.screen = StartingScreen(game)
            dispose()
        }


    }



    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        logo.dispose()

    }



}