package com.mygdx.game.screens


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.Screen
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.mygdx.game.*
import com.mygdx.game.`interface`.FontTtf


class StartingScreen(private val game: MainGame) : Screen,InputProcessor, FontTtf {
    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun keyTyped(character: Char): Boolean {return false}

    override fun scrolled(amount: Int): Boolean {return false}

    override fun keyUp(keycode: Int): Boolean {return false}

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false}

    override fun keyDown(keycode: Int): Boolean {
        return false}

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false}

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        game.screen = TownScreen(game)
        dispose()
        return true
    }

    private var camera: OrthographicCamera = OrthographicCamera()
    var theme: Music = Gdx.audio.newMusic(Gdx.files.internal("sounds/mainTheme.wav"))
    private val logo:Texture = game.getTexture(Textures.LOGORNW)
    val fontFile = Gdx.files.internal("pixFont.ttf")
    val stage = Stage()

    var fontStart :BitmapFont
   var fontCopyright :BitmapFont
    val logoImg:Image

    private var timeSeconds = 0f
    private val period = 0.1f
    private var fadeValue = -0.5f



    init {
        camera.setToOrtho(false, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())
        theme.isLooping = true
        theme.play()

        fontStart =createFont(Color.WHITE,screenWidth/200f,fontFile)
        fontCopyright = createFont(Color.WHITE,screenWidth/400f, fontFile )

        logoImg = Image(logo)
        logoImg.setPosition(screenWidth*.1f,screenHeight*1f)
        logoImg.setSize(logo.width*widthScale,logo.height*heightScale)
        val destinationX = screenWidth*.1f
        val destinationY = screenHeight*.6f

        logoImg.addAction(Actions.moveTo(destinationX, destinationY, 1f))
        stage.addActor(logoImg)


    }
    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun show() {
        Gdx.input.inputProcessor = this
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(60/255f, 55/255f, 55/255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined

        timeSeconds +=Gdx.graphics.rawDeltaTime
        if(timeSeconds > period) {
            timeSeconds -= period
            fadeValue += 0.05f
            if (fadeValue> 1.3f)
                fadeValue = .6f
        }

        //logoImg.act(Gdx.graphics.deltaTime)

       /* if(Gdx.input.isTouched) {
            tchd = true

        }*/


        stage.act(Gdx.graphics.deltaTime)
        stage.draw()



        game.batch.begin()

        game.batch.draw(logo,logoImg.x,logoImg.y,logoImg.width, logoImg.height)

        if (logoImg.actions.isEmpty) {
            fontStart.setColor(200f, 200f, 200f, fadeValue)
            fontStart.draw(game.batch, "Touch to start !", screenWidth * .16f, screenHeight * .4f)
        }
        fontCopyright.draw(game.batch,"Prototype v$VERSION_RNW",screenWidth*.05f,screenHeight*0.03f)
        game.batch.end()

    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        logo.dispose()
        theme.dispose()
    }




    //...Rest of class omitted for succinctness.

}