package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.mygdx.game.MainGame
import com.mygdx.game.Textures
import com.mygdx.game.screenHeight


class ParamScreen(private val game: MainGame, val screenBf:Screen) : Screen {

    private var camera: OrthographicCamera = OrthographicCamera()
    private val stage= Stage()

    private val txtureParam = game.getTexture(Textures.PARAM)
    var drawButParam   = TextureRegionDrawable(TextureRegion(txtureParam))
    val addButton = ImageButton(drawButParam)

    init {
        camera.setToOrtho(false, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())


        addButton.setPosition(50f,screenHeight/1.5f)
        addButton.image.setFillParent(true)
        addButton.setSize(120f,120f)
        addButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                addFun()
                super.clicked(event, x, y)
            }
        })

        stage.addActor(addButton)



    }
    fun addFun() {
        Gdx.input.getTextInput(object : Input.TextInputListener {
            override fun input(texteSaisi: String) {
                game.joueur.setNbGold(texteSaisi.toInt())
                /*val json = Json()
                val data = json.toJson(game.joueur)*/


                val file:FileHandle = Gdx.files.local("save.json")
                file.writeString(game.joueur.getNbGold().toString(),false)

            }

            override fun canceled() {

            }
        }, "add gold", "","add")
    }
    override fun hide() {}
    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(60/255f, 55/255f, 55/255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined

        game.batch.begin()

        game.batch.end()


        if(Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            game.screen = TownScreen(game); dispose()}

        stage.draw()
        stage.act()

    }


    override fun pause() {}


    override fun resume() {}

    override fun resize(width: Int, height: Int) {}

    override fun dispose() {

    }
}