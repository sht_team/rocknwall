package com.mygdx.game.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener
import com.mygdx.game.*
import com.mygdx.game.`interface`.FontTtf
import com.mygdx.game.actors.RessourceDisplay

import com.mygdx.game.stage.StageBgWall
import com.mygdx.game.stage.StageHeader


const val NB_BRIQUE = 22
//  butParam.style.imageDown = drawButGame

class WallScreen(internal val game: MainGame) : Screen, FontTtf {




    // load Texture
    val txtureHand = game.getTexture(Textures.HAND)
    val btnUpg_up = game.getTexture(Textures.BTNUP)
    val btnUpg_Down = game.getTexture(Textures.BTNDOWN)
    val pixFont = Gdx.files.internal("pixFont.ttf")







    // Image declaration
    val imgHand = Image(txtureHand)
    val imgHand2 = Image(txtureHand)
    val drawBtnUpg_up = TextureRegionDrawable(TextureRegion(btnUpg_up))
    val drawBtnUg_down = TextureRegionDrawable(TextureRegion(btnUpg_Down))






    var camera: OrthographicCamera
     var touchRect: Rectangle
     var font:BitmapFont
    var goldToAdd = 0



    var stageBg = StageBgWall()
    val stageHeader= StageHeader(game)
     var stageWall = game.stageWall
    val currentLvl = game.currentLvl


    val wall = game.wall
    val displayWall = game.dispWall

    val playButton = Button(drawBtnUg_down,drawBtnUg_down,drawBtnUpg_up)


    private var timeSeconds = 0f
    val font2 = createFont(Color.BLACK, screenWidth/350f,pixFont)
    val goldFont = createFont(Color.YELLOW, screenWidth/200f,pixFont)

    var add = false
    val input = WallInputProcessor(game,this)

    val resPwr:RessourceDisplay
    val resClick:RessourceDisplay


    init {


        touchRect = Rectangle()
        touchRect.x = 0f
        touchRect.y = screenHeight*.2f
        touchRect.width = screenWidth*1f
        touchRect.height = screenHeight*.5f

        camera = OrthographicCamera()
        camera.setToOrtho(false, screenWidth*1f, screenHeight*1f)

        val df = createFont(Color.BLACK, screenWidth/200f,pixFont)
        font = df



        playButton.setSize(btnUpg_up.width*1.5f*widthScale,btnUpg_up.height*1.5f*heightScale)
        playButton.setPosition(screenWidth*.67f, screenHeight*.02f)

        playButton.addListener(object : ActorGestureListener(){

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int) {
                    if (game.joueur.getNbGold()>= game.joueur.getUpgradePrice()) {
                        game.joueur.useGold(game.joueur.getUpgradePrice())
                        game.joueur.upPower()
                    }
                }

            override fun longPress(actor: Actor?, x: Float, y: Float): Boolean {
                while(game.joueur.getNbGold()>= game.joueur.getUpgradePrice()) {
                    game.joueur.useGold(game.joueur.getUpgradePrice())
                    game.joueur.upPower()
                }
                return super.longPress(actor, x, y)
            }
        })







        imgHand.setSize(txtureHand.width*1.2f*widthScale,txtureHand.height*1.2f*heightScale)
        imgHand.setPosition(screenWidth*.25f,screenHeight*.035f)

        imgHand2.setSize(txtureHand.width*1.2f*widthScale,txtureHand.height*1.2f*heightScale)
        imgHand2.setPosition(screenWidth*.7f,screenHeight*.7f)

        resPwr = RessourceDisplay(font,txtureHand,imgHand2,currentLvl.pwrNeeded,imgHand2.x,imgHand2.y)
        resClick = RessourceDisplay(font,txtureHand,imgHand,game.joueur.getClickPower(),imgHand.x,imgHand.y)

        stageHeader.addActor(resPwr)
        stageHeader.addActor(resClick)
        stageHeader.addActor(playButton)





    }


    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun show() {
        val multiplexer = InputMultiplexer()
        Gdx.input.inputProcessor = multiplexer
        //multiplexer.addProcessor(stage)
        multiplexer.addProcessor(stageHeader)
        multiplexer.addProcessor(input)
    }


    override fun render(delta: Float) {
      // Gdx.gl.glClearColor(54f/255, 165f/255, 182f/255, 1f)
       Gdx.gl.glClearColor(0.5f, .5f, .5f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined

        timeSeconds +=Gdx.graphics.rawDeltaTime
        if(timeSeconds > period) {
            game.timeLeft ++
            timeSeconds = 0f
        }

        playButton.isChecked = game.joueur.getNbGold() >= game.joueur.getUpgradePrice()


        resClick.updateRes(game.joueur.getClickPower())
        resPwr.updateRes(currentLvl.pwrNeeded)


        stageBg.draw()
        stageWall.act(Gdx.graphics.deltaTime)
        stageWall.draw()
        stageHeader.draw()


        game.batch.begin()

        displayWall.progressBar(game.batch,wall.getNbBrickSet())


        font.draw(game.batch, "Mur ${currentLvl.lvl}", screenWidth/2f, screenHeight*0.8f)
        font2.draw(game.batch, " +${game.joueur.getUpPower()}", screenWidth*.75f, screenHeight*0.11f)
        font2.draw(game.batch, "cost: ${game.joueur.getUpgradePrice()}", screenWidth*.75f, screenHeight*0.07f)


        if(displayWall.click_anim)
            displayWall.clickAnimation(game.batch)

        if(displayWall.gold_anim) {
            displayWall.goldAnimation(game.batch, stageWall)
            goldFont.draw(game.batch,"$goldToAdd",screenWidth*.57f, screenHeight*.57f)
        }



        game.batch.end()





        if (wall.getNbBrickSet() >= 22 && !wall.isFinished ) {
            goldToAdd = currentLvl.goldEarned
            // currentLvl.goNextLevel()
            wall.isFinished = true
            add = false

        }

       if(wall.isFinished) {
           if (!wall.anim && wall.listBrick[NB_BRIQUE-1].getBriqueImg().actions.isEmpty) {
               for (brick in wall.listBrick) {
                   brick.finishedAction()
               }
               displayWall.gold_anim = true
               wall.anim = true
              // imgPlat.addAction(Actions.sequence(Actions.moveTo(screenWidth*.1f, screenHeight*.15f+ screenHeight*.07f,.4f),Actions.moveTo(screenWidth*.1f, screenHeight*0f,.4f)))
                stageBg.moveGround()
           }


           if (wall.anim) {

               if (! displayWall.gold_anim && !add) {
                   game.joueur.addGold(goldToAdd)
                   add=true
               }


               if(wall.listBrick[21].getBriqueImg().actions.isEmpty) {
                   wall.finishedWall(stageWall)
                   stageBg.initGround()
                   currentLvl.goNextLevel()

               }
           }

        }
    }





    override fun pause() {
    }

    override fun resume() {
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun dispose() {
        stageBg.dispose()
        stageHeader.dispose()


    }

}