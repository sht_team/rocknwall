package com.mygdx.game.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.mygdx.game.*
import com.mygdx.game.`interface`.FontTtf
import com.mygdx.game.actors.headerRessources

class QuarryScreen(val game:MainGame): Screen, FontTtf {

    private var camera: OrthographicCamera = OrthographicCamera()
    private val stage= Stage()

    private val txtureParam: Texture = game.getTexture(Textures.PARAM)
    var drawButParam   = TextureRegionDrawable(TextureRegion(txtureParam))
    val addButton = ImageButton(drawButParam)
    private var timeSeconds = 0f
    val font = createFont(Color.BLACK, screenWidth/200f,Gdx.files.internal("pixFont.ttf"))


    init {
        camera.setToOrtho(false, Gdx.graphics.width.toFloat(), Gdx.graphics.height.toFloat())

        addButton.setPosition(50f, screenHeight /1.5f)
        addButton.image.setFillParent(true)
        addButton.setSize(120f,120f)
        addButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                game.joueur.addBrique(game.timeLeft*3)
                game.timeLeft = 0
                super.clicked(event, x, y)
            }
        })

        stage.addActor(headerRessources(game.joueur,game))
        stage.addActor(addButton)


    }
    override fun hide() {
        Gdx.input.inputProcessor = null
    }

    override fun show() {
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float) {
        timeSeconds +=Gdx.graphics.rawDeltaTime
        if(timeSeconds > period) {
            game.timeLeft ++
            timeSeconds = 0f
        }

        Gdx.gl.glClearColor(60/255f, 55/255f, 55/255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        camera.update()
        game.batch.projectionMatrix = camera.combined

        if(Gdx.input.isKeyPressed(Input.Keys.BACK)) {game.screen = TownScreen(game); dispose()}

        game.batch.begin()
        font.draw(game.batch,"Collect: ${game.timeLeft*3}", 50f, screenHeight /1.7f)
        game.batch.end()

        stage.draw()
        stage.act()



    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun resize(width: Int, height: Int) {

    }

    override fun dispose() {

    }


}

