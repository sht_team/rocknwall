package com.mygdx.game

import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.Gdx

import com.mygdx.game.stage.StageBgTown


class gestureList(val cam: OrthographicCamera, val stageBgTown: StageBgTown): GestureDetector.GestureListener {
    var panMax = 0f
    var panMax2 = 0f

    var delta=0f
    var flingVelocityY = 0f


    fun flingDecelerate(time: Float) {
        if ( this.flingVelocityY != 0f) {
           val newFlingY = Math.max(0f, Math.abs(this.flingVelocityY) - Math.abs(this.flingVelocityY) * 5f * time)
            if (this.flingVelocityY < 0)
                this.flingVelocityY = newFlingY * -1
            else
                this.flingVelocityY = newFlingY

            if (Math.abs(this.flingVelocityY) < 10) this.flingVelocityY = 0f

            if (this.flingVelocityY == 0f)
                Gdx.graphics.isContinuousRendering = false
        }
    }

    override fun fling(velocityX: Float, velocityY: Float, button: Int): Boolean {
        if (Math.abs(velocityY) > 100) {
            // start rendering continuously until fling done
            Gdx.graphics.isContinuousRendering = true
            this.flingVelocityY = velocityY * cam.zoom
        }
        return true
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        return false
    }

    override fun pinchStop() {
    }

    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        return false
    }

    override fun panStop(x: Float, y: Float, pointer: Int, button: Int): Boolean {

        return false
    }

    override fun longPress(x: Float, y: Float): Boolean {
        return true
    }

    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {

        //return (flingVelocityY!=0f)
       // return (flingVelocityY !in -100f..100f)
        return false
    }

    override fun pinch(initialPointer1: Vector2?, initialPointer2: Vector2?, pointer1: Vector2?, pointer2: Vector2?): Boolean {
        return false
    }




    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        stageBgTown.butHouse.isChecked = false
        stageBgTown.butMine.isChecked = false

        delta = deltaY



        if ( this.flingVelocityY != 0f) {
            this.flingVelocityY = 0f
            Gdx.graphics.setContinuousRendering(false)
        }

        return true

    }




}