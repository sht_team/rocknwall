package com.mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.math.Vector3
import com.mygdx.game.screens.NB_BRIQUE
import com.mygdx.game.screens.TownScreen
import com.mygdx.game.screens.WallScreen

class WallInputProcessor(val game:MainGame, val scr:WallScreen): InputProcessor{

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun keyTyped(character: Char): Boolean {return false}

    override fun scrolled(amount: Int): Boolean {return false}

    override fun keyUp(keycode: Int): Boolean {
        if (keycode== Input.Keys.BACK) {
            game.screen = TownScreen(game)
            scr.dispose()
        }
        return true
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false}

    override fun keyDown(keycode: Int): Boolean {
        return false}

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if(!scr.wall.isFinished && game.joueur.getNbBrique()>0) {
            val touch = Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
            scr.camera.unproject(touch)

            if(scr.touchRect.contains(touch.x,touch.y) && scr.wall.getNbBrickSet() < NB_BRIQUE  ) {
                scr.displayWall.x_click = touch.x
                scr.displayWall.y_click = touch.y
                scr.displayWall.click_anim = true
                var nbBrique = scr.currentLvl.click(game.joueur.getClickPower())
                if(nbBrique > game.joueur.getNbBrique())
                    nbBrique = game.joueur.getNbBrique()

                if (nbBrique> NB_BRIQUE-scr.wall.getNbBrickSet())
                    nbBrique = NB_BRIQUE-scr.wall.getNbBrickSet()

                scr.wall.brickToSet = nbBrique
                scr.wall.putBrick(scr.stageWall)
                game.joueur.useBrique(nbBrique)
            }
        }

        return true
    }

    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }


}