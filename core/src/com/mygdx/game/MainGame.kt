package com.mygdx.game

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.ObjectMap
import com.mygdx.game.`interface`.loadTextures
import com.mygdx.game.displays.DisplayWall
import com.mygdx.game.models.Level
import com.mygdx.game.models.Player
import com.mygdx.game.models.Wall
import com.mygdx.game.screens.logoScreen
import com.mygdx.game.actors.headerRessources


const val VERSION_RNW = 0.51
var period = 1f

val originalWidth = 1080f
val originalHeight = 1794f

var screenWidth = 0f
var screenHeight= 0f

var widthScale= 0f
var heightScale= 0f


class MainGame : Game(), loadTextures {

    lateinit var batch: SpriteBatch
    var joueur: Player = Player()
    lateinit var headerRessources: headerRessources

    //GameScreen
    lateinit var wall:Wall
    lateinit var stageWall:Stage
    val currentLvl = Level()
    lateinit var dispWall: DisplayWall
    lateinit var textureMap:ObjectMap<Textures,Texture>
    var timeLeft = 0





    override fun create() {
        screenWidth =  Gdx.graphics.width*1f
        screenHeight = Gdx.graphics.height*1f
        widthScale = screenWidth / originalWidth
        heightScale = screenHeight / originalHeight

        batch = SpriteBatch()
        Gdx.input.isCatchBackKey = true //back button don't close the game
        textureMap = loadAllTextures()
        dispWall = DisplayWall()
        wall = Wall()
        stageWall= Stage()
        headerRessources= headerRessources(joueur, this)
        this.screen = logoScreen(this)




        val file:FileHandle = Gdx.files.local("save.json")
        if (file.exists()) {
            val data = file.readString()
            joueur.setNbGold(data.toInt())
        }
        joueur.setNbBrique(100)
        //joueur.setNbGold(1000000)

       /*val json = Json()
        if(!data.isEmpty())
            joueur = json.fromJson<Player>(Player::class.java, data)*/

    }

    override fun getTexture(texture: Textures):Texture{
        return textureMap.get(texture)
    }

    override fun dispose() {
        batch.dispose()
        stageWall.dispose()

        for(t in textureMap.values())
            t.dispose()

    }



}

