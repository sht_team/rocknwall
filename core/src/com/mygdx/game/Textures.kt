package com.mygdx.game

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.utils.ObjectMap




enum class Textures(val fileName: String) {


    //header
    BG_HEADER("textures/barres.png"),
    GOLD("textures/gold.png"),
    DIAM("textures/diam.png"),
    BRICK("textures/briqueRes.png"),

    LOGORNW("textures/logo.png"),
    LOGOSHT("textures/SHT.png"),

    PARAM("textures/param.png"),
    BTNUP("textures/btn.png"),
    BTNDOWN("textures/btn1.png"),

    BGTOWN("textures/map.png"),
    SIGN("textures/sign.png"),
    HAND("textures/hand.png");



    fun get():Texture{ return Texture(fileName) }

}
fun loadAllTextures():ObjectMap<Textures,Texture>{
    val txtureMap = ObjectMap<Textures,Texture>()
    for(t in Textures.values())
        txtureMap.put(t,t.get())

    return txtureMap
}