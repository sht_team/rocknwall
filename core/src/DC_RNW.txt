@startuml


class Brick {
  -Texture txtureBrick
  -Float posX
  -Float posY
}


class Joueur {
  -int nbGold
  -int nbDiam
  -int nbBrick
}

class Power {
    int currentPow
    int lvlPow
    int upgradePrice

    void upPower(int value)
    void getLvlPow()
    void getCurrentPow()
}
class Wall {
  ArrayList<Brick> listBrick

  void moveWall(Stage stg)
  void putBrick(Stage stg)
  void getNbBrickSet()
}
note left: Initialize the bricks

class Level {
  Int lvl
  Int pwrNeeded
  -Int remainingClick
  Int goldEarned
  boolean isBoss

  void goNextLevel()
  void click()
}


class DisplayGame {
  void ressources(batch: SpriteBatch, font: BitmapFont,txtureImg:Texture, img:Image, nbRes:Int, posX:Float, posY:Float)
  BitMapFont createfont(Color color, Float size, FileHandle fileFont)


}

class DisplayWall {
  void progressBar(Int nbBrickSet)
  void upgradeButton(Int price, Int power)
}

class Actor
class headerRessources


Joueur "1" --> Power
DisplayWall --|> DisplayGame
Wall *-- Brick

headerRessources --|> Actor
@enduml